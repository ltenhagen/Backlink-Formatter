<!doctype html>
<html lang="en">
  <head>
    <title>Startpagina Backlink Formatter</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.0.2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  </head>
  <body>

    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-8">
                <div id="request" class="card">
                    <div class="card-body">
                        <div id="info">
                            <h3 class="card-title text-center">Backlink Formatter</h3>
                            <p class="card-text text-center">Voer hieronder je URL(s) in van de startpagina links.</p>
                        </div>
                        <div id="form">
                            <div class="mb-3">
                              <label for="url" class="form-label">URL(s)</label>
                              <textarea class="form-control" name="urls" id="urls" rows="2" placeholder="jouwbegin.nl, zoekned.nl, ..."></textarea>
                            </div>

                            <div class="d-grid gap-2">
                              <button type="button" name="generate" id="generate" class="btn btn-primary">Genereer backlinks</button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div id="response" class="card visually-hidden">
                    <div class="card-body">
                        <div id="info">
                            <h4 class="card-title text-center">Backlink(s)</h4>
                        </div>
                        <div id="table">
                            <table id="response">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap JavaScript Libraries -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="application/javascript">
        $(document).ready(function () {
            $('button#generate').on('click', function () {
                let urls = $('textarea#urls').val();

                if (!urls) {
                    return alert('Voer een URL in.');
                }

                $.ajax({
                    type: 'POST',
                    url: '/api/format',
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}'
                    },
                    data: {
                        urls: urls
                    },
                    success: function (data) {
                        $('div#response').removeClass('visually-hidden');
                        $('table#response tr').remove();

                        data.backlinks.forEach(function (data) {
                            console.log(data);
                            
                            let row = '<tr><td>' + data + '</td></td>';
                            $('table#response').append(row);
                        })
                    },
                    error: function (data) {
                        return alert(data.responseJSON.message);
                    }
                });
            });
        });
    </script>

  </body>
</html>
