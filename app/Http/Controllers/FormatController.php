<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormatController extends Controller
{
    public function format(Request $request) {
        if (!$request->has('urls')) {
            return response([
                'type' => 'error',
                'message' => 'Er is iets mis gegaan, probeer het opnieuw.'
            ], 400);
        }

        // $urls = str_replace('#l', "\n", \request()->get('urls'));
        $urls = explode("\n", \request()->get('urls'));

        $backlinks = [];
        foreach ($urls as $url) {
            $parsedURL = parse_url($url);

            if (!array_key_exists('query', $parsedURL) || !array_key_exists('host', $parsedURL)) {
                return response()->json([
                    'message' => 'Er is iets fout gegaan, contacteer de beheerder.'
                ], 422);
            }

            $category = $parsedURL['query'];
            $url = $parsedURL['host'];

            if (!$category || !$url) {
                return response()->json([
                    'message' => 'Er is iets fout gegaan, contacteer de beheerder.'
                ], 422);
            }

            $category = str_replace('page=', '', $category);
            $backlink = 'https://' . $category . '.' . $url;

            $backlinks[] = $backlink;
        }

        return response()->json([
            'backlinks' => $backlinks
        ], 200);
    }
}
